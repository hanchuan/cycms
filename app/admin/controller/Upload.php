<?php
namespace app\admin\controller;

use think\facade\View;
use think\facade\Request;

class Upload extends Common
{
    public function uploadpic($name='image', $width='100', $height='100', $url='')
    {
        $url = base64_decode($url);
        $file = $this->uploadsave($name);
        if ($file) {
            $url = $file;
        }
        View::assign('name', $name);
        View::assign('width', $width);
        View::assign('height', $height);
        View::assign('url', $url);
        return View::fetch();
    }

	public function uploadpics($name='images', $url=''){
        if (Request::isPost()) {
			$files = input('post.');
			$url = [];
			$i=0;
			foreach($files as $k=>$v){
				$file = request()->file('file'.$i);
				$i++;
				try {
					$savename = \think\facade\Filesystem::disk('public')->putFile('',$file);
					$url[] = str_replace("\\","/",'/static/upload/'.$savename);
				} catch (think\exception\ValidateException $e) {
					//echo $e->getMessage();
					$savename = false;
				}
				
			}
			if(!empty($url)){
				return json(['code'=>1,'url'=>$url,'urls'=>implode('|',$url),'msg'=>'']);
			}else{
				return json(['code'=>0,'url'=>[],'msg'=>'上传失败']);
			}
            
        }
		$url = base64_decode($url);
        View::assign('url', $url);
        View::assign('images', $url<>''?explode('|',$url):[]);
        View::assign('name', $name);
        return View::fetch();
	}
	
    private function uploadsave($name)
    {
        if (Request::isPost()) {
            $file = request()->file($name);
            try {
                $savename = \think\facade\Filesystem::disk('public')->putFile('',$file);
                $savename = str_replace("\\","/",'/static/upload/'.$savename);
            } catch (think\exception\ValidateException $e) {
                //echo $e->getMessage();
                $savename = false;
            }
            
            return $savename;
        }
    }
}
