<?php
return [
    '用户登录'=>'用户登录',
    '记住我'=>'记住我',
    '登录'=>'登录',
    '用户名'=>'用户名',
    '用户密码'=>'用户密码',
    '验证码'=>'验证码',
    '看不清楚？点击刷新'=>'看不清楚？点击刷新',
];
